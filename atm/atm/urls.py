
from django.conf.urls import url, include
from django.contrib import admin
from login import views

app_name = 'logi'
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^lock', views.view_lock, name='lock'),
    url(r'^$', views.view_login, name='login_page'),
    url(r'^signup', views.view_signup, name='register'),
    url(r'^logout/', views.view_logout, name='logout'),
    url(r'^home/', views.home, name='home'),
    url(r'^depost/', views.depost, name='depost'),
    url(r'^withdraw/', views.withdraw, name='withdraw'),
    url(r'^status/', views.status, name='status')
]
