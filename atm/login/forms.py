from django import forms
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(label=_("Username"), max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Username'),
                                                             'required': '', 'tabindex': 1, 'autofocus': '1'}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                                      'placeholder': _('*******'),
                                                                                      'required': '', 'tabindex': 2}))

class SignUpForm(forms.Form):
    username = forms.CharField(label=_("Username"), max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('    Username'),
                                                             'required': '', 'tabindex': 1, 'autofocus': '1'}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                                      'placeholder': _('    *******'),
                                                                                      'required': '', 'tabindex': 2}))

    email = forms.CharField(label=_("Email"), widget=forms.TextInput(attrs={'class': 'form-control',
                                                                                      'placeholder': _('    Email'),
                                                                                      'required': '', 'tabindex': 2}))
    name = forms.CharField(label=_("Name"), widget=forms.TextInput(attrs={'class': 'form-control',
                                                                                      'placeholder': _('    Name'),
                                                                                      'required': '', 'tabindex': 2}))

    amount = forms.CharField(label=_("Amount"), widget=forms.NumberInput(attrs={'class': 'form-control',
                                                                                      'placeholder': _('    Multiple of 500'),
                                                                                      'required': '', 'tabindex': 2}))


class withdrawal(forms.Form):
    withdrawal = forms.CharField(label=_('Withdrawal'), max_length=100,
                            widget=forms.TextInput(attrs={'class': 'form-control system-search', 'required': '',
                                                          'tabindex': 1, 'autofocus': '1',
                                                          'placeholder': _('Amount')}))

class lockScreen(forms.Form):
    group = forms.CharField(label=_('LockScreen'), max_length=100,
                            widget=forms.PasswordInput(attrs={'class': 'form-control system-search', 'required': '',
                                                          'tabindex': 1, 'autofocus': '1',
                                                          'placeholder': _('********')}))

class deposit(forms.Form):
    deposit = forms.CharField(label=_('Deposit'), max_length=100,
                            widget=forms.TextInput(attrs={'class': 'form-control system-search', 'required': '',
                                                          'tabindex': 1, 'autofocus': '1',
                                                          'placeholder': _('Amount')}))

class transfer(forms.Form):
    number = forms.CharField(label=_('ATM No. or Account no.'), max_length=100,
                            widget=forms.TextInput(attrs={'class': 'form-control system-search', 'required': '',
                                                          'tabindex': 1, 'autofocus': '1',
                                                          'placeholder': _('Enter 16 Digit ATM No. or 11 Digit Account No.')}))

    amount = forms.CharField(label=_('Amount'), max_length=100,
                            widget=forms.TextInput(attrs={'class': 'form-control system-search', 'required': '',
                                                          'tabindex': 1, 'autofocus': '1',
                                                          'placeholder': _('Amount')}))
