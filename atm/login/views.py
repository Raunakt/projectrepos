from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignUpForm, lockScreen, withdrawal, transfer, deposit
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse
import csv
from django.contrib.auth.models import User
from datetime import datetime
import re
from random import randint
from django.http import HttpResponse
from .models import operation, Details

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    query = None
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        query = or_query
    return query

def view_lock(request):
    form = lockScreen()
    error = False
    if request.method == "POST":
        form = lockScreen(request.POST)
        if form.is_valid():
            password = form.cleaned_data["group"]
            pas = request.user.username
            user = authenticate(username=pas, password=password)
            if user:
                login(request, user)
                return redirect('home')
            else:
                error = True
                return render(request, 'login/lockscreen.html', {'form' : form, 'error': error})
    return render(request, 'login/lockscreen.html', {'form' : form, 'error': error})

def depost(request):
    er = False
    er1 = False
    de = Details.objects.get(user_id=request.user)
    bal = de.bal
    if request.method == "POST":
        form = deposit(request.POST)
        if form.is_valid():
            bal1 = form.cleaned_data["deposit"]
            bal1 = int(bal1)
            if bal1%100 != 0:
                er1 = True
            else:
                bal2 = bal +  bal1
                de.bal = bal2
                de.save()
            txform = transfer()
            dpost = deposit()
            wid = withdrawal()
            tx = operation.objects.filter(Q(user_id=request.user)).order_by('-id')
            de = Details.objects.get(user_id=request.user)
            arg = {'txform': txform, 'dpost': dpost, 'with': wid, 'operation': tx, 'details': de, 'error3': er1,}
            return render(request, 'login/lists_contacts.html', arg)

    return render(request, 'login/lists_contacts.html' )


def withdraw(request):
    er = False
    er1 = False
    de = Details.objects.get(user_id=request.user)
    bal = de.bal
    if request.method == "POST":
        form = withdrawal(request.POST)
        if form.is_valid():
            bal1 = form.cleaned_data["withdrawal"]
            bal1 = int(bal1)
            if bal1>bal:
                er = True
            elif bal1%100 != 0:
                er1 = True
            else:
                bal2 = bal -  bal1
                de.bal = bal2
                de.save()
            txform = transfer()
            dpost = deposit()
            wid = withdrawal()
            tx = operation.objects.filter(Q(user_id=request.user)).order_by('-id')
            de = Details.objects.get(user_id=request.user)
            arg = {'txform': txform, 'dpost': dpost, 'with': wid, 'operation': tx, 'details': de, 'error2': er, 'error1':er1}
            return render(request, 'login/lists_contacts.html', arg)

    return render(request, 'login/lists_contacts.html' )


def home(request):
    txform = transfer()
    dpost = deposit()
    wid = withdrawal()
    tx = operation.objects.filter(Q(user_id=request.user)).order_by('-id')
    de = Details.objects.get(user_id=request.user)
    arg = {'txform':txform, 'dpost': dpost, 'with': wid, 'operation': tx, 'details':de}
    return render(request, 'login/lists_contacts.html', arg)


def view_login(request):
    error = False
    login_form = LoginForm()

    if request.user.is_authenticated():
        return redirect('lock')

    if request.method == "POST":
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = login_form.cleaned_data["username"]
            password = login_form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('home')
            else:
                error = True
    return render(request, 'login.html', {'form' : login_form, 'error': error})


def view_signup(request):
    signup_form = SignUpForm()
    if request.method == "POST":
        signup_form = SignUpForm(request.POST)
        if signup_form.is_valid():
            username = signup_form.cleaned_data["username"]
            password = signup_form.cleaned_data["password"]
            email = signup_form.cleaned_data["email"]
            name = signup_form.cleaned_data["name"]
            bal = signup_form.cleaned_data["amount"]
            atmn = randint(1000000000000000, 9999999999999999)
            acn = randint(10000000000, 99999999999)
            cvc = randint(100, 999)
            user1 = User.objects.create_user(username, email, password)
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
            det = Details.objects.create(name = name, acn = acn, atmn = atmn, cvc = cvc, bal = bal, user_id=request.user)
            return HttpResponse("You Have signup......Thanks")
    return render(request, 'signup.html', {'form' : signup_form })


def view_logout(request):
    logout(request)
    return redirect('login_page')



def status(request):
    er = False
    er1 = False
    if request.method == "POST":
        g = 0
        ur = 0
        form = transfer(request.POST)
        if form.is_valid():
            number = form.cleaned_data["number"]
            amount = form.cleaned_data["amount"]
            user = Details.objects.filter(atmn = number)
            user1 = Details.objects.filter(acn=number)
            if user:
                g = 1
            if user1:
                g = 2

            if g == 1:
                ur = Details.objects.get(atmn=number)

            if g == 2:
                ur = Details.objects.get(acn=number)

            name = ur.name
            ac = ur.acn
            atm = ur.atmn
            userid = ur.user_id


            de = Details.objects.get(user_id=request.user)
            name1 = de.name
            ac1 = de.acn
            atm1 = de.atmn

            bal = de.bal
            amount = int(amount)
            if g != 1 and  g!=2:
                er = True

            elif amount > bal:
                er1 = True

            else:
                bal2 = bal -  amount
                de.bal = bal2
                de.save()

                dde = operation.objects.create(name = name, acn = ac, atmn = atm, amount = amount, status = 'Transfer', user_id = request.user)
                bal1 = ur.bal
                bal2 = bal1 + amount
                ur.bal = bal2
                ur.save()


                dde1 = operation.objects.create(name=name1, acn=ac1, atmn=atm1, amount=amount, status='Received',
                                              user_id=userid)


            txform = transfer()
            dpost = deposit()
            wid = withdrawal()
            tx = operation.objects.filter(Q(user_id=request.user)).order_by('-id')
            de = Details.objects.get(user_id=request.user)
            arg = {'txform': txform, 'dpost': dpost, 'with': wid, 'operation': tx, 'details': de, 'error4': er, 'error5': er1}
            return render(request, 'login/lists_contacts.html', arg)
    return render(request, 'login/lists_contacts.html')







