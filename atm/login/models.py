from django.db import models
from django.contrib.auth.models import User


class operation(models.Model):
    name = models.CharField(verbose_name="name", max_length=100)
    acn = models.EmailField(verbose_name="acn", max_length=100, null = True)
    atmn = models.CharField(verbose_name="atmn", max_length=100)
    amount = models.CharField(verbose_name="amount", max_length=100)
    status = models.CharField(max_length=30)
    user_id = models.CharField(max_length=2)

    def __str__(self):
        return self.name


class Details(models.Model):
    name = models.CharField(max_length=128)
    acn = models.CharField(max_length=20)
    atmn = models.CharField(max_length=50)
    cvc = models.CharField(max_length=10, null=True)
    bal = models.IntegerField()
    user_id = models.ForeignKey(User)

    def __str__(self):              # __unicode__ on Python 2
        return self.name
